'use strict'; /* global describe, it  */
var ghCloneAll = require('..');
var assert = require('assert');

describe('gh-clone-all', function() {
  describe('repositoryNames', function() {
    it('runs without passing on an error', function() {
      return ghCloneAll.repositoryNames('yamadapc')
        .then(function(repos) {
          assert(repos.length > 0);
        });
    });
  });
});
