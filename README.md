gh-clone-all
============
A simple script to clone all public repositories from a Github user.

## Installing
```
npm install -g gh-clone-all
```

## Usage
```
$ gh-clone-all -h

  Usage: index [-l] <username>

  Options:

    -h, --help     output usage information
    -V, --version  output the version number
    -l, --list     Only lists the repositories
$
```

## License
This code is licensed under the GPLv2 license for Pedro Table Yamada. For more
information, please refer to the [LICENSE](/LICENSE) file.
