#!/usr/bin/env node
'use strict';
var Promise = require('bluebird');
var program = require('commander');
var ProgressBar = require('progress');
var ghCloneAll = require('..');

var pkgJson = require('../package.json');

module.exports = exports = main;

if(!module.parent) {
  main()
    .catch(function(err) {
      console.error(err.message);
      process.exit(err.code || 1);
    });
}

function main() {
  program.name('gh-clone-all');
  program.version(pkgJson.version);

  program
    .usage('[-l] <username>')
    .option('-l, --list', 'Only lists the repositories')
    .parse(process.argv);

  if(!program.args.length) {
    program.help();
    process.exit(1);
    return;
  }

  if(program.list) {
    return Promise.map(program.args, function(user) {
      return ghCloneAll.repositoryNames(user)
        .then(function(repos) {
          repos.forEach(function(repo) {
            console.log(program.user + '/' + repo);
          });
        });
    });
  }

  return Promise.map(program.args, function(user) {
    return ghCloneAll.repositoryNames(user)
      .then(function(repos) {
        var pg = new ProgressBar('Cloning [:bar] :percent :etas', {
          complete: '=',
          incomplete: ' ',
          width: 80,
          total: repos.length
        });

        return Promise.map(repos, function(repo) {
          return ghCloneAll.gitClone(
            'git@github.com:' + user + '/' + repo,
            user + '/' + repo
          ).then(function() {
            pg.tick();
          });
        });
      });
  });
}
