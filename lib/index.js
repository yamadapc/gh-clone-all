'use strict';
var child_process = require('child_process');
var Promise = require('bluebird');
var request = require('superagent');

Promise.promisifyAll(request.Request.prototype);

/**
 * Fetches the list of repositories in a github user account
 */

exports.repositoryNames = function repositoryNames(username) {
  return request
    .get('https://api.github.com/users/' + username + '/repos')
    .endAsync()
    .then(function(res) {
      if(res.status !== 200) {
        throw new Error(JSON.parse(res.text).message);
      }

      var repos = JSON.parse(res.text);
      var names = repos.map(function(repo) {
        return repo.name;
      });

      return names;
    });
};

/**
 * Clones a repository into a destination
 */

exports.gitClone = function gitClone(url, targetDir) {
  return execAsync('git', ['clone', url, targetDir])
    .then(function() {
      process.stdout.clearLine();
      process.stdout.cursorTo(0);
      console.log('Cloned ' + url + ' to ' + targetDir);
    });
};

/**
 * A siplified version of exec that returns a promise
 */

function execAsync(cmd, args) {
  return new Promise(function(fulfill, reject) {
    var proc = child_process.spawn(cmd, args);
    proc.on('close', function(code) {
      if(code !== 0) {
        reject(new Error('`' + cmd + '`` exited with: ' + code));
      }

      fulfill();
    });
  });
}
